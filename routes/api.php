<?php

use Illuminate\Support\Facades\Route;
use Digitall\Aiora\OrderService\Http\Controllers\OrderController;

Route::post('order' , [OrderController::class, 'create'])->name("create");
Route::get('order' , [OrderController::class, 'listAll'])->name("listAll");

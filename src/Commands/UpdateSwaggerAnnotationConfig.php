<?php
namespace Digitall\Aiora\OrderService\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\File;


class UpdateSwaggerAnnotationConfig extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'aiora:updateSwagger';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Annotation config in swagger';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $annotation_path = config('l5-swagger.documentations.default.paths.annotations');
        $annotation_path[] = base_path( 'vendor'. DIRECTORY_SEPARATOR.
                                         'digitall'.DIRECTORY_SEPARATOR.
                                         'aiora-order-service');
        config(['l5-swagger.documentations.default.paths.annotations' => $annotation_path]);
        config(['l5-swagger.documentations.default.paths.docs_json' => 'schema.json']);


        $storageSchemaPath = dirname(__FILE__).DIRECTORY_SEPARATOR.
                             '..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.
                             'postman'.DIRECTORY_SEPARATOR.'schemas';

        File::ensureDirectoryExists($storageSchemaPath);

        config(['l5-swagger.defaults.paths.docs' => $storageSchemaPath]);

        Artisan::call('l5-swagger:generate');
    }
}

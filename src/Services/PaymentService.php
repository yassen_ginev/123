<?php
declare (strict_types=1);

namespace Digitall\Aiora\OrderService\Services;

use Digitall\Aiora\OrderService\Events\OrderCreated;
use Digitall\Aiora\OrderService\Http\Requests\OrderPostRequest;
use Digitall\Aiora\OrderService\Http\Responses\OrderResponse;
use Digitall\Aiora\OrderService\Models\Order;
use Illuminate\Queue\Queue;
use Illuminate\Queue\SqsQueue;


class PaymentService
{


    private $request;

    /**
     * PaymentService constructor.
     * @param OrderPostRequest $request
     */
    public function __construct(OrderPostRequest $request)
    {
        $this->request = $request;
    }

    public function process() : OrderResponse
    {

        if($this->request->get("process") == 'false'){
            return new OrderResponse( "Payment failed" ,  424   , '');
        }

        $orderId = (string)rand();

        $ord = new Order($orderId);
        OrderCreated::dispatch(new Order($orderId));


        return new OrderResponse("" , 200  , ["orderId" => $orderId]);
    }

}

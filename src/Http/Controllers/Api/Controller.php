<?php
namespace Digitall\Aiora\OrderService\Http\Controllers\Api;

class Controller
{
    /**
     * @OA\Info(
     *      version="1.0.1",
     *      title="Order service API",
     *      description="L5 Swagger OpenApi description",
     *      @OA\Contact(
     *          email="admin@admin.com"
     *      ),
     *      @OA\License(
     *          name="Apache 2.0",
     *          url="http://www.apache.org/licenses/LICENSE-2.0.html"
     *      )
     * )
     *
     */
}

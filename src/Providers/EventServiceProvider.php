<?php
namespace Digitall\Aiora\OrderService\Providers;

use Digitall\Aiora\OrderService\Events\OrderCreated;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Digitall\Aiora\OrderService\Listeners\OnOrderCreated;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [
        OrderCreated::class => [
            OnOrderCreated::class
        ]
    ];


    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
    }
}

<?php
namespace Digitall\Aiora\OrderService\Events;


use Digitall\Aiora\OrderService\Models\Order as Order;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;


class OrderCreated
{

    use Dispatchable, SerializesModels;

    /**
     * The order instance.
     *
     * @var \Digitall\Aiora\OrderService\Models\Order
     */
    public $order;

    /**
     * Create a new event instance.
     *
     * @param  \Digitall\Aiora\OrderService\Models\Order  $order
     * @return void
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * @return mixed
     */
    public function getPayload()
    {
        return [
            'event' => 'CreateDuo',
            'data' => $this->order
        ];
    }
}
